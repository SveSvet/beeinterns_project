const greetingsText = 'Hello, World!';
const container = document.querySelector('#container');
const textElem = document.createElement('p');

textElem.innerText = greetingsText;
container.append(textElem);
